
// CONFIG
#pragma config FOSC = XT        // Oscillator Selection bits (XT oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)


#include <xc.h>
#include <stdio.h>
#include <pic16f873a.h>
#include <math.h>



// Oscillator configuration
#define _XTAL_FREQ 3686400
// ADC configuration
#define ADC_NUM_CHANNELS 3
#define PRELOAD_VALUE (256 - ((_XTAL_FREQ / 16) * 0.000833)

unsigned char RX_value;
char To_be_recieved_first[5] = "Send";
char To_be_sent_first[4] = "Yes";
char MAX_I_values[4];
uint8_t RX_BUF, Index_RX = 0 ,k=1,Tx_bit=0,Rx_bit=0, i,Tx_sample=0,Rx_sample=0,j=0;
uint8_t start_of_send = 0,detect=0;
int MAX_I, I,value_TX=170,value_RX=0,Rx_byte;
uint8_t order_of_recive = 0,order_send=0;
uint8_t fram_Length = 0,PREV_RC4=0;
uint8_t start_bit=0;
//int tx_s[10];
 int I_mesure[3];

uint8_t ptr0;


int reverseBits(int num);

void interrupt ISR()
{

   if((INTCONbits.INTF == 1) && (INTCONbits.INTE == 1))
    {   
        RC4=1;
        INTCONbits.INTF = 0;
        
           RC0 = 1;
          //  RB1 = 1;
           RC5 = 1;
           order_send = 1;
           
         TMR0=175; 
         T0IE = 1;

         //TMR0=PRELOAD_VALUE; 

    }     
    
    
    if (T0IF  )
    {  
      
//    //  RC0=1;
        TMR0=175;
        if (Rx_sample==4)
        {
           
        
            Rx_sample=0;
        }    
    if (Tx_sample==4)
        {           
       

            Tx_sample=0;
        }       
         if ((RC4 == 0 )&&(detect == 0)&&(PREV_RC4))
            
        {    RB2=1;  
             
                Rx_bit=0;
                detect=1;
                Rx_sample=0; 
             RB2=0;  
        }                
        PREV_RC4=RC4;
     
        
          if ((detect==1)&&(Rx_sample==2))
            
        {
                 RB1=1;  
      //  RC2 = 1;   
           if ( ((Rx_bit<9)&&(Rx_bit>0)) )
           {          // RC0 = 1;
               
               RX_BUF= RC4;
             value_RX=(RX_BUF<<(Rx_bit-1))|value_RX;
            // value_RX=value_RX + RC4;  
                
            
           }
           if (Rx_bit==9 && RC4==1)
           {
             //    RC1=0;
               RX_BUF=0;
               detect = 0 ; 
          I_mesure[Rx_byte]= value_RX;
           value_RX=0;
             Rx_byte++;
               Rx_bit=0;
               if (Rx_byte==4)
                   Rx_byte=1;
               

           }
           if(detect == 1 )
           {  
               Rx_bit++;
           
           }
        
       // RC2 = 0;
          RB1=0;  
        }   
        
        // TMR0 =(256 - (_XTAL_FREQ / 128) * 0.000833); 
      
        
     if (order_send==1)
        {
         
              
            if(Tx_sample==4 || Tx_sample==0)
            {
                Tx_sample=0;
                 if (Tx_bit==0)
             {
      
            
                RC3=0;
                //j++; 
                
             }
           
             if ((Tx_bit>0)&&(Tx_bit<9)  )
             {      
            
          //  RC2 = 1;
               RC3 = value_TX-((value_TX >> 1)<<1);
                value_TX >>= 1;
            // RC4=1;  //  j++;
              
              } 
             if (Tx_bit==9) 
             {
                    order_send=0;   
                 Tx_bit=0;
                RC3=1;
                //j++; 
                value_TX=170;
             RC5 = 0;

             }
              if( order_send == 1 ) 
                 Tx_bit++;
            }
             
            // RC1 = 0;           
          

             
             
             
         //  RC2 = 0;
        }
         Tx_sample++;
         
         Rx_sample++;
         
         // RC1=0;
        T0IF = 0; // Clear the Timer0 interrupt flag

        //if (j==0)
      //  RC0=0;  
       //  RC0 = 0;
    }
}
int reverseBits(int num) {
    int numOfBits = sizeof(num) * 8;
    int reverseNum = 0;
    int i;
   
    for (i = 0; i < numOfBits; i++) {
        if((num & (1 << i)))
           reverseNum |= 1 << ((numOfBits - 1) - i);  
    }
   
    return reverseNum;
}

void main(void) {
    // Configure USART
//    TRISC = 0xC0;
//    TXSTA = 0x04;
//    RCSTA = 0x90;
//    SPBRG = 0x03;
//    TXSTAbits.TXEN = 1;     // enable the transmit operation.
//    //PIE1bits.TXIE = 1;      // transmit interrupt enable.
//    PIE1bits.RCIE = 1;      // Receive interrupt enable.
    // when we want to transmit, we need to write the value to TXREG
    // when we want to read the received value, we need to read the value from RCREG
    
    __delay_ms(1000);
        // Configure Timer0
      OPTION_REG = 0x00; // Prescaler 1:2
   TMR0 = 175; // Reload the timer with the preload value
 // Set the preload value
    T0IE = 1; // Enable Timer0 interrupt
     
    
    
    
    // Configure External Interrupt
    OPTION_REGbits.INTEDG = 1;  // rising edge  external interrupt
    INTCONbits.INTE = 1;
    INTCONbits.INTF = 0; // the flag bit of the external interrupt must be cleared in the program in the ISR.
            
    
    // Configure ADC
    ADCON0=0;
    ADCON0bits.ADCS0 = 0b1; // Set ADC conversion clock to Fosc/16
    ADCON0bits.ADCS1 = 0b0; // Set ADC conversion clock to Fosc/16  ADCS1 =1
    ADCON1=0;
    ADCON1bits.PCFG = 0b0010; // Set all analog pins as analog inputs (0 1 2 3 4 )and vree+=vdd and vreef-=vss
    ADCON1bits.ADFM = 0b1; // Set all analog pins as analog inputs (0 1 2 3 4 )and vree+=vdd and vreef-=vss
    ADCON1bits.ADCS2=0b1;
    ADCON0bits.ADON = 1; // Turn on ADC module
    
    
    // global and peripheral interrupt enable
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;
    
    // Configure I/O
     TRISA = 0b000000;  //   TRISA = 0;
   // TRISBbits.TRISA4 = 0;   // IsTherePower

    
    TRISBbits.TRISB1 = 0;   // IsTherePower
    TRISBbits.TRISB3 = 0;   // SensorPower3
    TRISBbits.TRISB4 = 0;   // SensorPower2
    TRISBbits.TRISB5 = 0;   // SensorPower1
    TRISBbits.TRISB2 = 0;   // IsTherePower

    TRISCbits.TRISC3 = 0;   // SleepPowerEnable
    TRISCbits.TRISC4 = 1;   // SleepRXEnable
    TRISCbits.TRISC5 = 0;   // DE/RE   
    PORTA=0;
    //RA4 = 0;
    RB3 = 1;
    RB4 = 1;
    RB5 = 1;
    RC3 = 1;
  //  RC4 = 1;
    RC5 = 1;
    RB1 = 0;
    
     TRISCbits.TRISC0 = 0;   // SleepPowerEnable
     RC0 = 0;
     RB2=0;
     RC4=1;
     
  

     
    while (1)
    {
       
        //__delay_ms(10);
 //  PORTA = value_RX >>2;
   
    PORTA = I_mesure[1]  ;
  //PORTA =16;
   //PORTA = 170>>2;
   // RA4=1;
 
    }
    
}


